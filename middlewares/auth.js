const User = require("../model/users");
const jwt = require("jsonwebtoken");
const JWT_SECRET = "addjsonwebtokensecretherelikeQuiscustodietipsoscustodes";

exports.authorize = async (req, res, next) => {
    try {
        const headerToken = req.headers.authorization.split(" ")[1];
        let token = jwt.verify(headerToken, JWT_SECRET);
        req.user = await User.findById(token.userId);
        
        next();
    } catch (error) {
        res.status(401).json({message: "Authentication failed!"});
    }
};

