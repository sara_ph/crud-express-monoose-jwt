const router = require("express").Router();

router.use("/users", require("./users/router.js"));
router.use("/services", require("./services/router.js"));

module.exports=router;