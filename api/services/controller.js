// ////* Reqires
const Service = require("../../model/services.js");
// //////////////////*
// //////////* Controllers
// ////* Create
exports.createService = async function (req, res) {
    let newService = await Service.create(req.body);
    return res.status(201).send(newService);
}
// ////* Read
exports.readServices = async (req, res) => {
    const services = await Service.find();
    return res.send(services);
}
// ////* Update
exports.updateService = async (req, res) => {
    let id = req.params.id;
    if (await Service.findByIdAndUpdate(id, {$set: req.body})) {
        res.status(200).json({statusCode: 200, statusMessage: "OK"});
    } else {
        res.status(404).json({error_code: 404, error_message: "Not Found"});
    }
}
// ////* Delete
exports.deleteService = async function (req, res) {
    let id = req.params.id;
    if (await Service.findByIdAndRemove(id)) {
        res.status(200).json({statusCode: 200, statusMessage: "OK"});
    } else {
        res.status(404).json({error_code: 404, error_message: "Not Found"});
    }
}