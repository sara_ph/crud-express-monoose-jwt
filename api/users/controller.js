// ////* Reqires
const bcrypt = require("bcrypt");
const User = require("../../model/users");
const jwt = require('jsonwebtoken');
const JWT_SECRET = "addjsonwebtokensecretherelikeQuiscustodietipsoscustodes";

// ////////////* Controllers
// ////* Create
exports.createUser = async function (req, res) { // //// Duplicate Username
    if ((await User.findOne({username: req.body.username})) !== null) {
        return res.status(406).json({errorCode: 406, errorMessage: "Bad Request: Username Is Duplicate"});
    }
    // // Hash password
    try {
        let hashed = await hashPassword(req.body.password)
        req.body.password = hashed;
    } catch (ex) {
        res.send(ex.message);

    }
    // //// Create new user
    try {
        let user = await User.create(req.body);
        return res.status(201).send(user);
    } catch (ex) {
        return res.send(ex.message);
    }
}
// ////* Login (Generating JWT)
exports.loginUser = async (req, res) => {
    let user;
    // ////  Check the existence of the user
    if (user = await User.findOne({username: req.body.username})) { // ////  Check password
        if (await checkPassword(req.body.password, user.password)) {
            let jwtToken = jwt.sign({ // / Generate jwt
                userId: user._id
            }, JWT_SECRET, {expiresIn: "1h"});
            res.status(200).json({token: jwtToken, expiresIn: 3600, _id: user._id});
        } else {
            res.status(401).json({errorCode: 401, errorMessage: "The password is incorrect!"});
        }
    } else {
        res.status(404).json({errorCode: 404, errorMessage: "Not Found"});
    }

}
// ////* Read
exports.readUsers = async (req, res) => {
    const users = await User.find();
    return res.send(users);
}
// ////* Update
exports.updateUser = async (req, res) => { // //// Duplicate Username
    if ((await User.findOne({username: req.body.username})) !== null) {
        return res.status(406).json({errorCode: 406, errorMessage: "Bad Request: Username Is Duplicate"});
    }
    // /// Hashing
    await hashPassword(req.body.password).then((hashed) => {
        req.body.password = hashed;
    }).catch((err) => {
        res.send(err);
    });
    // //// Updating
    if (await User.findByIdAndUpdate(req.params.id, {$set: req.body})) {
        return res.status(200).json({statusCode: 200, statusMessage: "OK"});
    } else {
        return res.status(404).json({errorCode: 404, errorMessage: "Not Found"});
    }
}
// ////* Delete
exports.deleteUser = async function (req, res) {
    if (await User.findByIdAndRemove(req.params.id)) {
        res.status(200).json({statusCode: 200, statusMessage: "OK"});
    } else {
        res.status(404).json({errorCode: 404, errorMessage: "Not Found"});
    }
}
exports.permissionCheck = (req, res, next) => {
    if ((req.user._id).toString() !== req.params.id) {
        return res.status(403).json({errorMessage: "permission denied!"})
    }
    next();
}
// //////////* Functions
async function hashPassword(password) {
    const saltRounds = 10;
    const hashedPassword = await new Promise((resolve, reject) => {
        bcrypt.hash(password, saltRounds, function (err, hash) {
            if (err) 
                reject(err);
            


            resolve(hash);
        });
    });

    return hashedPassword;
}
async function checkPassword(password, hashed) {

    return new Promise((resolve) => {
        bcrypt.compare(password, hashed, function (err, res) {
            if (err) 
                resolve(false);
            


            if (res) { // Passwords match
                resolve(true);
            } else { // Passwords don't match
                resolve(false);
            }
        });
    });
}
